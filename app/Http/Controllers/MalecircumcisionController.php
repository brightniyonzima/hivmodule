<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\smcfollowup;
use Yajra\Datatables\DataTables;

class MalecircumcisionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  
         if(request()->ajax())
        {
            return datatables()->of(smcfollowup::latest()->get())
                ->addColumn('action',function($smcfollowup){
                   
                    $button = '<a href="'. route('malecircumsion.edit', $smcfollowup->id) .'" class="edit btn btn-primary btn-sm" type="submit">Edit</a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '  <button class="btn-delete btn btn-danger" data-remote="/malecircumsion/' .  $smcfollowup->id . '">Delete</button>';
                    return $button;
               
                })
                ->addColumn('more',function($smcfollowup){
                $button = '<a href="'. route('malecircumsion.show', $smcfollowup->id) .'" class="more btn  btn-primary btn-sm" type="submit"><i class="glyphicon glyphicon-trash"></i> More</a>';
                return $button;
               
            })
                ->rawColumns(['action','more'])
                ->make(true);
        }
        
        return view('smcfollowuptable');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('smcfollowup');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
       $this->validate($request,[
            'types'=>'required',
            'cprovider'=>'required:smcfollowups',
            'serid'=>'required',
            'today'=>'required',
            'wstatus'=>'required', 
            'visit'=>'required',
            'AE'=>'required',
            'severity'=>'required',
            'ttgiven'=>'required',
       ]);

       $smcfollowup = new smcfollowup;
       $smcfollowup->sitetype= $request->types;
       $smcfollowup->CTL = $request->cprovider;
       $smcfollowup->NatID = $request->natid;
       $smcfollowup->SerID= $request->serid ;
       $smcfollowup->Date= $request->today ;
       $smcfollowup->wound = $request->wstatus;
       $smcfollowup->missedapt  = $request->missedapt;
       $smcfollowup->visittype = $request->visit;
       $smcfollowup->AE = implode(",",$request->AE);
       $smcfollowup->severity = $request->severity;
       $smcfollowup->tmtgiven = $request->ttgiven; 
       $smcfollowup->save();
       
       session()->flash('message','Record Inserted Successfully');
        return redirect('/malecircumsion');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $smcfollowup=smcfollowup::find($id);
        return view('morefollowup');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $smcfollowup=smcfollowup::find($id);
        // dd($smcfollowup);


        // isset($smcfollowup->AE) ? $smcfollowup->AE : 'AE'

        // if (isset($smcfollowup->AE)) {
        //     $smcfollowup->AE
        // } else {
        //     'AE'
        // }
        return view('edit',compact('smcfollowup'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $smcfollowup= smcfollowup::find($id);
        $this->validate($request,[
             'types'=>'required',
             'cprovider'=>'required:smcfollowups',
             'serid'=>'required',
             'today'=>'required',
             'wstatus'=>'required', 
             'visit'=>'required',
             'AE'=>'required',
             'severity'=>'required',
             'ttgiven'=>'required',
        ]);
        
        $smcfollowup->sitetype= $request->types;
        $smcfollowup->CTL = $request->cprovider;
        $smcfollowup->NatID = $request->natid;
        $smcfollowup->SerID= $request->serid ;
        $smcfollowup->Date= $request->today ;
        $smcfollowup->wound = $request->wstatus;
        $smcfollowup->missedapt  = $request->missedapt;
        $smcfollowup->visittype = $request->visit;
        $smcfollowup->AE = implode(",",$request->AE);
        $smcfollowup->severity = $request->severity;
        $smcfollowup->tmtgiven = $request->ttgiven; 
        $smcfollowup->save();
        session()->flash('message','Record Updated Successfully');
         return redirect('/malecircumsion');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $smcfollowup=smcfollowup::find($id);
        $smcfollowup->delete();
        session()->flash('message','Deleted Successfully');
        return redirect('malecircumsion');
    }
}
