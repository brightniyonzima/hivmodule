<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OPDpatient extends Model
{
    protected $fillable=['visit_date','serial_number','muac_cm','mauc','weight','height','bmi','age_of_weight_zscore','height_of_weight_zscore','blood_pressure_Systolic','blood_pressure_diastolic','blood_sugar','next_of_kin','palliative_Care','patient_classification','tobacco','fever','test_done', 'results','results_of_new_presumed_case','lab_test_results','linked_to_TB','doses_per_day','time2','disability','type_of_disability','device_provided','refferal_in_number','refferal_out_number','drug','diagnosis','time1','units_per_dose'];
	protected $dates =['created_at'];
}
