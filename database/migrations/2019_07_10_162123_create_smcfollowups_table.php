<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmcfollowupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('smcfollowups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sitetype');
            $table->string('CTL');
            $table->string('NatID');
            $table->string('SerID');
            $table->date('Date');
            $table->string('wound');
            $table->string('missedapt');
            $table->string('visittype');
            $table->string('AE');
            $table->string('severity');
            $table->string('tmtgiven');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('smcfollowups');
    }
}
