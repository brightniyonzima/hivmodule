<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
    ];
});
<<<<<<< HEAD
=======
$factory->define(App\smcfollowup::class, function (Faker $faker) {
   return [
'sitetype' =>  str_random(10),
'CTL' => str_random(10),
'NatID' => str_random(10),
'SerID' => str_random(10),
'Date' => Carbon::now(),
'wound' => str_random(10),
'missedapt' => str_random(10),
'visittype' => str_random(10),
'AE' => str_random(10),
'severity' => str_random(10),
'tmtgiven' => str_random(10),
   ];
 });
>>>>>>> 3d3e0cc0c6f7793d150b4491b6fca302e74faced
