@extends('layouts.app')

@section('table3')
<div class="container">
 @include('message')
    <a href="/malecircumsion2" class="btn btn-info">Back</a>
        <h2 class="pull-center">More About A Patient</h2>
        <table class="table table-bordered" id="users-table">
    <thead>
    <tr class="headers" style="text-align: center;">

    <th colspan="4" scope="colgroup">Eligibility For Circumcision</th>
    <th colspan="18" scope="colgroup">Circumcision Procedure</th>
    <th colspan="18" scope="colgroup">Post Operation Management</th>
    
    </tr>
    <tr>
              
                <th scope="col">Informed Consent For SMC Given</th>
                <th scope="col">Eligibility After History/Physical Exam</th>
                <th scope="col">Circumcision Date</th>
                <th scope="col">Time Started</th>
                <th scope="col">End Time</th>
                <th scope="col">AnesthesiaType(Lignocaine 1%mls)</th>
                <th scope="col">AnesthesiaType(Bupivicaine 0.25%mls)</th>
                <th scope="col">Type of Procedure</th>
                <th scope="col">Any Other</th>
                <th scope="col">Prepex</th>
                <th scope="col">Size</th>
                <th scope="col">ShangRing</th>
                <th scope="col">Size</th>
                <th scope="col">Circumciser</th>
                <th scope="col">Cadre</th>
                <th scope="col">Assistant</th>
                <th scope="col">Cadre</th> 
                <th scope="col">Adverse Of Events</th>
                <th scope="col">Severity</th>
                <th scope="col">Treatment Given</th>
                <th scope="col">BP:Systolic</th>
                <th scope="col">BP:Diastolic</th>
                <th scope="col">Pulse</th>
                <th scope="col">RR</th>
                <th scope="col">PP Medication Given</th>
               
        
    </tr>
</thead>
<tbody>
</table>
</div>
@endsection
@push('scripts')
<script>
$(document).ready(function(){
    $('#users-table').DataTable({
        processing:true,
        serverSide:true,
        ajax:{
            url:"{{route('malecircumsion2.index')}}",
        },
        columns:[
            {
                data:'ic_smc',
                name:'ic_smc',

            },
            {
                data:'eligible',
                name:'eligible',
            },
            {
                data:'date_of_cmsn',
                name:'date_of_cmsn',
            },
            {
                data:'time_started',
                name:'time_started',
            },
            {
                data:'end_time',
                name:'end_time',
            },
            {
                data:'lignocaine',
                name:'lignocaine',
            },
            {
                data:'bupivicaine',
                name:'bupivicaine',
               
            },
            {
                data:'typeofcircumcision',
                name:'typeofcircumcision',
                
            },
            {
                data:'others',
                name:'others',
                
            },
            {
                data:'prepex',
                name:'prepex',
                
            },
            {
                data:'size',
                name:'size',
            
            },
            {
                data:'shangring',
                name:'shangring',
                
            },
            {
                data:'sizze',
                name:'sizze',
                
            },
            {
                data:'circumciser',
                name:'circumciser',
                
            },
            {
                data:'cadre',
                name:'cadre',
                
            },
            {
                data:'assistant',
                name:'assistant',
                
            },
            {
                data:'cadr ',
                name:'cadr ',
                
            },
            {
                data:'events_during_proc',
                name:'events_during_proc',
                
            },
            {
                data:'severit',
                name:'severit',
            
            },
            {
                data:'Tmtgiven ',
                name:'Tmtgiven ',
                
            },
            {
                data:'systo',
                name:'systo',
                
            },
            {
                data:'diasto',
                name:'diasto',
                
            },
            {
                data:'PULS',
                name:'PULS',
                orderable:false
            },
            {
                data:'R',
                name:'R',
                
            },
            {
                data:'LPO_medic',
                name:'LPO_medic',
            
            }
        ]
    });
});
</script>
@endpush