@extends('layouts.app')

@section('content')
{{ Form::open(['url'=>'OPD','method'=>'post'])}}

<div class='headers' style="text-align: center"> 
	<h2 style="background: #c0c0c0, padding:10px">OUT PATIENT DEPARTMENT(OPD)FORM</h2>
</div>
<div class="container-fluid">
  @include('OPD.error')
  <fieldset>
    <table width="100%">
    </table>
      <div class="form-row">
        <div class="col-auto">
          {{ Form::label('','Visit date') }}
        </div>
        <div class="col-1.5">
          {{ Form::date('visit_date','',['class'=>'form-control',]) }}
        </div>
        <div class="col-4"></div>
        <div class="col-auto">
          {{ Form::label('','Serial number') }}
        </div>
        <div class="col-3">
          {{ Form::text('serial_number','',['class'=>'form-control',]) }}
        </div>
      </div>
    </table>
  </fieldset>
  <br> 
  <fieldset> 
    <h3>Vitals</h3>
      <table>
        <div class="form-row">
          <div class="col-auto">
            {{ Form::label('','M.U.C.A(cm)') }}
          </div>
          <div class="col-1">
            {{ Form::text('muac_cm','',['class'=>'form-control']) }}
          </div> 
          <div class="col-1"></div> 
          <div class="col-auto">
            {{ Form::label('','M.A.U.C') }}
          </div>
          <div class="col-2">
            
             {{ Form::select('mauc',['','Red<19.0cm','Yellow 19.0cm - 22.0cm','Green >= 22.0cm'],['0'],['class'=>'form-control']) }}
          </div>
          <div class="col-2"></div>
          <div class="col-auto">
            {{ Form::label('','Weight')}}
          </div>
          <div class="col-1">
            {{ Form::text('weight','',['class'=>'form-control'])}} 
          </div>
          <div class="col-auto">
            {{ Form::label('','Height/length(cm)')}}
          </div>
          <div class="col-1">
            {{ Form::text('height','',['class'=>'form-control'])}}
          </div>    
        </div>
        <br>

        <div class="form-row">
          <div class="col-auto">
            {{ Form::label('','BMI')}}
          </div>
          <div class="col-1">
            {{ Form::text('bmi','',['class'=>'form-control'])}} 
          </div>
           
          <div class="col-auto">
            {{ Form::label('','Age of weight Z-score')}}
          </div>
          <div class="col-4">
            {{ Form::select('age_of_weight_zscore',['','Normal'=>'Normal','Over weight'=>'Over weight','Under weight'=>'Under weight','Unknown'=>'Unknown','Malnutrition of moderate degree(Gomez: 60% to less than 75% of standard weight)'=>'Malnutrition of moderate degree(Gomez: 60% to less than 75% of standard weight)','Severe protein-calorie malnutrition(Gomez: less than 60% of standard weight)'=>'Severe protein-calorie malnutrition(Gomez: less than 60% of standard weight)','Malnutrition os standard degree(Gomez: 75% to less than 90% of standard weigt)'=>'Malnutrition os standard degree(Gomez: 75% to less than 90% of standard weigt)','Undernourished'=>'Undernourished'],['0'],['class'=>'form-control'])}}
          </div>
          <div class="col-1"></div>
          <div class="col-auto">
            {{ Form::label('','Height of age Z-score')}}
          </div>
          <div>
            {{-- <input type="radio" name="gender" value="Normal-n">
            <input type="radio" name="gender" value="Underweight-u"> --}}
            {{ Form::radio('height_of_age_zscore','Normal-n')}}<label>Normal-n</label>
            {{ Form::radio('height_of_age_zscore','Underweight-u')}}<label>Underweight-u</label>
          </div>
        </div>
        <br>
        <div class="form-row">
          <div class="col-2">
            {{ Form::label('','Blood pressure-Systolic (mmg)')}}
            {{ Form::text('blood_pressure_systolic','',['class'=>'form-control'])}}
          </div>
          <div class="col-2"></div>
          <div class="col-2.5">
            {{ Form::label('','Blood pressure_Diastolic (mmg)')}}
            {{ Form::text('blood_pressure_diastolic','',['class'=>'form-control'])}}
          </div>
          <div class="col-1"></div>
          <div class="auto">
            {{ Form::label('','Blood sugar(mmg)')}}
          </div>
          <div class="col-1">
            {{ Form::text('blood_sugar','',['class'=>'form-control'])}}
          </div>
          <div>
            {{ Form::label('','Temp(C)'),['class'=>'col-auto']}}
          </div>
          <div class="col-1">
            {{ Form::text('temp','',['class'=>'form-control'])}}
          </div>
        </div>
      </table>
  </fieldset>
  <br>
  <fieldset>
    <table>
      <h3>Follow information</h3>
      <div class="form-row">
        <div class="col-auto">
          {{ Form::label('','Next of kin')}}
        </div>
        <div class="col-3">
          {{ Form::text('next_of_kin','',['class'=>'form-control'])}}
        </div>
        <div class="col-auto">
          {{ Form::label('','Need of palliative care?')}}
        </div>
        <div>
          {{ Form::radio('palliative_care','yes')}}<label> yes</label>
          {{ Form::radio('palliative_care','no')}}<label> yes</label>
        </div>
        <div class="col-1"></div>
        <div class="col-auto">
          {{ Form::label('','Patient classification')}}
        </div>
        <div>
          {{ Form::radio('patient_classification','New attaendance')}}<label> New Attendance</label>
          {{ Form::radio('patient_classification','Re-attendance')}}<label> Re-attendance</label>
        </div>
      </div>
      <br>
      <div class="form-row">
        <div class="col-auto">
          {{ Form::label('','Tobacco use?')}}
        </div>
        <div>
          {{ Form::radio('tobacco','Yes')}}<label>Yes</label>
          {{ Form::radio('tobacco','No')}}<label>no</label>
        </div>
        <div class="col-3"></div>
        <div class="col-auto">
          {{ Form::label('','Alcohol use?')}}
        </div>
        <div>
          {{ Form::radio('alcohol','yes')}}<label> yes</label>
          {{ Form::radio('alcohol','no')}}<label> no</label>
          </div>
        
      </div>
    </table>
  </fieldset>
  <br>
  <fieldset>
    <h3>Malaria </h3>
    <div class="form-row">
      <div class="col-auto">
        {{ Form::label('','Fever')}}
      </div>
      <div>
        {{ Form::radio('fever','yes')}}<label> yes</label>
        {{ Form::radio('fever','no')}}<label> No</label>
      </div>
      <div class="col-1"></div>
      <div class="col-auto">
        {{ Form::label('','Test done')}}
      </div>
      <div>
        {{ Form::radio('test_done','Blood smear/Miscroscopy')}}<label>Blood smear/Microscopy</label>
        {{ Form::radio('test_done','Rapid diagnoistic test')}}<label> Rapid diagnoistic test</label>
        {{ Form::radio('test_done','Not done')}}<label> Not done</label>
      </div>
      <div class="col-1"></div>
      <div class="col-auto">
        {{ Form::label('','Results')}}
      </div>
      <div>
        {{ Form::radio('results','positive')}}<label> Positive</label>
        {{ Form::radio('results','negative')}}<label> Negative</label>
        {{ Form::radio('results','not done')}}<label> Not done</label>
      </div>
      
    </div>
  </fieldset>
  <br>
  <fieldset>
    <h3>TB</h3>
    <div class="form-row">
      <div class="col-auto">
        {{ Form::label('','New Presumed case')}}
      </div>
      <div>
        {{ Form::radio('results_of_new_presumed_case','yes')}}<label> yes</label>
        {{ Form::radio('results_of_new_presumed_case','no')}}<label> no</label>
      </div>
      <div class="col-1"></div>
      <div class="col-auto">
        {{ Form::label('','Sent to lab?')}}
      </div>
      <div>
        {{ Form::checkbox('sent_to_lab','yes')}}
      </div>
      <div class="col-1"></div>
      <div class="col-auto">
        {{ Form::label('','LAB test Results')}}
      </div>
      <div>
        {{ Form::radio('lab_test_results','positive')}}<label>Postive</label>
        {{ Form::radio('lab_test_results','negative')}}<label>Negative</label>
        {{ Form::radio('lab_test_results','N/A')}}<label> N/A</label>
      </div>
      <div class="col-1"></div>
      <div class="col-auto">
        {{ Form::label('','Linked to TB')}}
      </div>
      <div>
        {{ Form::radio('linked_to_TB','yes')}}<label> yes</label>
        {{ Form::radio('linked_to_TB','no')}}<label> no</label>
      </div>
    </div>
  </fieldset>
  <br>
  <fieldset>
    <div>
      <h3>New diagnosis</h3>
      
    </div>
    
    <table class="multi" width="100%">
      <tbody>
        <tr>
          <td style="width: 5%">1</td>
          <td style="width: 70%" >
            <p>
              {{ Form::text('diagnosis[]','',['class'=>'form-control'])}}
            </p>
          </td>
          <td>
            <a href="#" class="add">Add diagnosis </a>
            <a href="#" class="remove">Remove diagnosis </a>
          </td>
        </tr>
        
        </tr>
        

      </tbody>
    </table>
  </fieldset>
  <br>
  <fieldset>
    <h3>Drugs</h3>
    <table class="multi" width="100%" >
      <thead>
        <tr>
          <th >Drug</th>
          <th style="width: 10%">Unit per dose</th>
          <th>Doses per day</th>
          <th>Duration</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        
        <tr>
          <td>
            {{ Form::text('drug[]', '', ['class' => 'form-control'] )}} 
          </td>
          <td>
            {{ Form::text('units_per_day[]','',['class'=>'form-control']) }}
          </td>
          <td>
            {{ Form::select('doses_per_day',['','Every 30min','Every hour','Every four hours','Every five hours','Every eight hours','Every forty eight hours','Monday,Wednesday and Friday'],['0'],['class'=>'form-control'])}}
          </td>
          <td class="form-control">
            <p>
          
                {{ Form::text('time1[]','')}}
                {{ Form::select('time2',['','seconds','minutes','hours','days','weeks','months','number of occurences'],['0'])}}
              
            </p>
               
          </td>
          <td>
            <a href="#" class="add">Add </a>
            <a href="#" class="remove">Remove</a>
          </td>

        </tr>
        
      </tbody>
    </table>
  </fieldset>
  <br>
  <fieldset>
    <h3>Disability</h3>
      <table>
        <tbody>
          <tr>
            <td>
                {{ Form::label('','Disability')}}
                {{ Form::radio('disability','yes')}}<label> yes</label>
                {{ Form::radio('disability','no')}}<label> No</label>
            </td>
            <td>
              <{{ Form::label('','Type of Disability')}}
              {{ Form::checkbox('type_of_disability','visual impairment')}}<label> Visual Impairment</label>
              {{ Form::checkbox('type_of_disability','speech disability')}}<label>Speech disability</label>
              {{ Form::checkbox('type_of_disability','walking disability')}}<label> Walking disability</label>
              {{ Form::checkbox('type_of_disability','mental/Illectual disabilities')}}<label> mental/Illectual disabilities</label>
              
            </td>
            <td>
              {{ Form::label('Device provided')}}
              {{ Form::checkbox('device_provided','Spectacles')}}<label> Spectacles</label>
              {{ Form::checkbox('device_provided','Wheelchair')}}<label> Wheelchair</label>
              {{ Form::checkbox('device_provided','Walking stick')}}<label>Walking stick</label>
              {{ Form::checkbox('device_provided','Hearing aid')}}<label> Hearing aid</label>
              {{ Form::checkbox('device_provided','Walker')}}<label>Walker</label>
            </td>

          </tr>
        </tbody>
      </table>
    </div>
  </fieldset>
  <br>
  <fieldset>
    <h3>Out patient Refferals</h3>
    <div class="form-row">
      <div class="col-auto">
        <label>Referral in number</label>
      </div>
      <div class="col-2">
        {{ Form::text('refferal_in_number','',['class'=>'form-control'])}}
      </div>
      <div class="col-4"></div>
      <div class="col-auto">
        <label>Referral out number</label>
      </div>
      <div class="col-2">
        {{ Form::text('refferal_out_number','',['class'=>'form-control'])}}
      </div>
    </div>
  </fieldset>

  <br>
  <div>
    {!! Form::button(isset($model)?'Update':'save',['class'=>'btn btn-success', 'type'=>'submit'])!!}
  </div>

</div> 



{{ Form::close()}}

@endsection